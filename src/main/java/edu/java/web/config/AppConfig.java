package edu.java.web.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@ImportResource("config.xml")
public class AppConfig{
    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
