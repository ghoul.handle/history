package edu.hanoi.controller;

import edu.hanoi.model.Dynasty;
import edu.hanoi.model.InfoKing;
import edu.hanoi.services.SaveAndSearchService;
import edu.hanoi.services.impl.SaveAndSearchServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/api/search")
public class SearchController {
    private final SaveAndSearchService saveAndSearchService;

    public SearchController(SaveAndSearchServiceImpl searchService) {
        this.saveAndSearchService = searchService;
    }

    @GetMapping("/dynasties")
    public ResponseEntity<List<Dynasty>> searchDynasties(@RequestParam String keyword) {
        return ResponseEntity.status(HttpStatus.OK).body(saveAndSearchService.searchDynasties(keyword));
    }

    @GetMapping()
    public ResponseEntity<List<Object>> searchAll(@RequestParam String keyword) {
        return ResponseEntity.status(HttpStatus.OK).body(saveAndSearchService.searchAll(keyword));
    }

    @GetMapping("/kings")
    public ResponseEntity<List<InfoKing>> searchKings(@RequestParam String keyword) {
        return ResponseEntity.status(HttpStatus.OK).body(saveAndSearchService.searchKings(keyword));
    }
}
