package edu.hanoi.controller;

import edu.hanoi.services.SaveAndSearchService;
import edu.hanoi.services.impl.SaveAndSearchServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/api/data")
public class GetDataController {
    private final SaveAndSearchService saveAndSearchService;

    public GetDataController(SaveAndSearchServiceImpl searchService) {
        this.saveAndSearchService = searchService;
    }

    @GetMapping
    public ResponseEntity<Boolean> getDaTa() throws IOException {
        saveAndSearchService.indexDataFromWeb();
        return ResponseEntity.status(HttpStatus.OK).body(true);
    }
}
