package edu.hanoi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.logging.Logger;


@Controller
public class HomeController {
    private static final Logger LOGGER = Logger.getLogger(String.valueOf(HomeController.class));

    @RequestMapping("/hello")
    @ResponseBody String home(){
        return "Hello World!";
    }

    @RequestMapping("/")
    ModelAndView begin(){
        ModelAndView mv = new ModelAndView("begin");
        LOGGER.info("Da truy cap vao day");
        return mv;
    }

    @RequestMapping("/data")
    ModelAndView showData(@RequestParam boolean isSuccess) {
        ModelAndView mv = new ModelAndView("show.data");
        if (isSuccess) {
            LOGGER.info("Da truy cap vao day");
        }
        return mv;
    }

}
