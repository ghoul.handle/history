package edu.hanoi.model;


import java.util.List;

public class Dynasty {
    private String time;
    private List<String> king;
    private String nameDynasty;
    private String dataMatch;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<String> getKing() {
        return king;
    }

    public void setKing(List<String> king) {
        this.king = king;
    }

    public String getNameDynasty() {
        return nameDynasty;
    }

    public void setNameDynasty(String nameDynasty) {
        this.nameDynasty = nameDynasty;
    }

    public String getDataMatch() {
        return dataMatch;
    }

    public void setDataMatch(String dataMatch) {
        this.dataMatch = dataMatch;
    }
}
