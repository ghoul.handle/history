package edu.hanoi.model;

import lombok.Data;

import java.util.List;

public class HistoryDynasty {
    private String time;
    private List<String> king;
    public HistoryDynasty(String time, List<String> king) {
        this.time = time;
        this.king = king;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<String> getKing() {
        return king;
    }

    public void setKing(List<String> king) {
        this.king = king;
    }
}
