package edu.hanoi.model;

import java.util.List;

public class Data {

    public Data(List<InfoKing> infoKings, List<Dynasty> dynasties) {
        this.infoKings = infoKings;
        this.dynasties = dynasties;
    }
    private List<InfoKing> infoKings;
    private List<Dynasty> dynasties;

    public List<InfoKing> getInfoKings() {
        return infoKings;
    }

    public List<Dynasty> getDynasties() {
        return dynasties;
    }

    public void setDynasties(List<Dynasty> dynasties) {
        this.dynasties = dynasties;
    }

    public void setInfoKings(List<InfoKing> infoKings) {
        this.infoKings = infoKings;
    }
}
