package edu.hanoi.model;

import lombok.Data;

@Data
public class InfoKing {
    private String stateBrand;
    private String name;
    private String information;
    private String time;
    private String emperorName;
    private String dataMatch;

    public String getStateBrand() {
        return stateBrand;
    }

    public void setStateBrand(String stateBrand) {
        this.stateBrand = stateBrand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEmperorName() {
        return emperorName;
    }

    public void setEmperorName(String emperorName) {
        this.emperorName = emperorName;
    }

    public String getDataMatch() {
        return dataMatch;
    }

    public void setDataMatch(String dataMatch) {
        this.dataMatch = dataMatch;
    }
}
