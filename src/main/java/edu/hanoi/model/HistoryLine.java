package edu.hanoi.model;

import lombok.Data;

import java.util.ArrayList;

public class HistoryLine {
    private ArrayList<String> historyDynasty;

    public ArrayList<String> getHistoryDynasty() {
        return historyDynasty;
    }

    public void setHistoryDynasty(ArrayList<String> historyDynasty) {
        this.historyDynasty = historyDynasty;
    }
}
