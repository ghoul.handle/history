package edu.hanoi.services;

import edu.hanoi.model.Dynasty;
import edu.hanoi.model.InfoKing;

import java.io.IOException;
import java.util.List;

public interface SaveAndSearchService {
    List<Dynasty> searchDynasties(String keyword);
    List<Object> searchAll(String keyword);
    List<InfoKing> searchKings(String keyword);
    void indexDataFromWeb() throws IOException;
}
