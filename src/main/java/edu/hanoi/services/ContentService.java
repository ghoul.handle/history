package edu.hanoi.services;

import edu.hanoi.model.Dynasty;
import edu.hanoi.model.InfoKing;
import org.jsoup.nodes.Document;

import java.util.List;

public interface ContentService {
    List<InfoKing> getAllInfoKing(Document document);
    List<Dynasty> getAllDynasty(Document document);
}
