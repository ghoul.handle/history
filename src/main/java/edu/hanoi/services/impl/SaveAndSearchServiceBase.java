package edu.hanoi.services.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.hanoi.model.Dynasty;
import edu.hanoi.model.InfoKing;
import edu.hanoi.util.VietnameseConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SaveAndSearchServiceBase {
    private final ObjectMapper objectMapper;
    private static final Logger LOGGER = LoggerFactory.getLogger(SaveAndSearchServiceBase.class);
    private String sourceFile;
    private static final Pattern patternENCharacter = Pattern.compile("^[a-zA-Z0-9 ]+$");

    public SaveAndSearchServiceBase(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        sourceFile = System.getProperty("user.dir") + "/files/";
        createDirectoryIfNotExist(sourceFile);
    }

     protected List<InfoKing> makeKingsResponse(String keyword, List<InfoKing> kings) {
        for (InfoKing king : kings) {
            String emperorName = king.getEmperorName();
            String emperorNameEN = VietnameseConverter.toTextNotMarked(king.getEmperorName());
            int index = emperorNameEN.toLowerCase().indexOf(VietnameseConverter.toTextNotMarked(keyword.toLowerCase()));
            if (index < 0) continue;
            int end = emperorName.length() > keyword.length() ? keyword.length() : emperorName.length();
            king.setDataMatch(emperorName.substring(index, index + end));
        }
        return kings;
    }

    protected boolean isVietnameseCharacter(String data) {
        Matcher matcher = patternENCharacter.matcher(data);
        return !matcher.matches();
    }


    protected List<Dynasty> makeDynastyResponse(String keyword, List<Dynasty> dynasties) {
        for (Dynasty dynasty : dynasties) {
            String nameDynasty = dynasty.getNameDynasty();
            String nameDynastyEN = VietnameseConverter.toTextNotMarked(dynasty.getNameDynasty());
            int index = nameDynastyEN.toLowerCase().indexOf(VietnameseConverter.toTextNotMarked(keyword.toLowerCase()));
            if (index < 0) continue;
            int end = nameDynasty.length() > keyword.length() ? keyword.length() : nameDynasty.length();
            dynasty.setDataMatch(nameDynasty.substring(index, index + end));
        }
        return dynasties;
    }


    protected boolean isMatchWithVICharacter(String source, String key) {
        String sourceToEqual = source.toLowerCase();
        String keyToEqual = key.toLowerCase();
        return sourceToEqual.contains(keyToEqual);
    }

    protected boolean isMatch(String source, String key) {
        String sourceToEqual = VietnameseConverter.toTextNotMarked(source);
        String keyToEqual = VietnameseConverter.toTextNotMarked(key);
        sourceToEqual = sourceToEqual.replaceAll("[^a-zA-Z0-9\"\"]", "");
        keyToEqual = keyToEqual.replaceAll("[^a-zA-Z0-9\"\"]", "");
        sourceToEqual = sourceToEqual.toLowerCase();
        keyToEqual = keyToEqual.toLowerCase();
        return sourceToEqual.contains(keyToEqual);

    }

    protected void toFile(String fileName, Object data) {
        File file = new File(sourceFile + fileName + ".json");
        try {
            objectMapper.writeValue(file, data);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    protected<T> List<T> fromFile(String fileName, TypeReference<List<T>> typeReference) {
        File file = new File(sourceFile + fileName + ".json");
        try {
            return objectMapper.readValue(file, typeReference);
        } catch (Exception e) {
            LOGGER.warn(e.getMessage(), e);
        }
        return new ArrayList<>();
    }

    private void createDirectoryIfNotExist(String path) {
        File theDir = new File(path);
        if (!theDir.exists()){
            theDir.mkdirs();
        }
    }
}
