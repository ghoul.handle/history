package edu.hanoi.services.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.hanoi.model.Data;
import edu.hanoi.model.Dynasty;
import edu.hanoi.model.InfoKing;
import edu.hanoi.services.ContentService;
import edu.hanoi.services.SaveAndSearchService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SaveAndSearchServiceImpl extends SaveAndSearchServiceBase implements SaveAndSearchService {
    private final ContentService contentService;

    public SaveAndSearchServiceImpl(ObjectMapper objectMapper, ContentService contentService) {
        super(objectMapper);

        this.contentService = contentService;
    }

    List<Dynasty> dynastiesSearchCache = new ArrayList<>();
    List<InfoKing> kingsSearchCache = new ArrayList<>();


    @Override
    public List<Dynasty> searchDynasties(String keyword) {
        if (dynastiesSearchCache.isEmpty()) {
            TypeReference<List<Dynasty>> typeReference = new TypeReference<List<Dynasty>>() {
                @Override
                public Type getType() {
                    return super.getType();
                }
            };
            dynastiesSearchCache.addAll(fromFile("dynasties", typeReference));
        }
        List<Dynasty> result = new ArrayList<>();
        if (isVietnameseCharacter(keyword)) {
            result.addAll(dynastiesSearchCache.stream()
                    .filter(dynasty -> isMatchWithVICharacter(dynasty.getNameDynasty(), keyword))
                    .collect(Collectors.toList()));
        } else {
            result.addAll(dynastiesSearchCache.stream()
                    .filter(dynasty -> isMatch(dynasty.getNameDynasty(), keyword))
                    .collect(Collectors.toList()));
        }
        return makeDynastyResponse(keyword, result);
    }

    @Override
    public List<Object> searchAll(String keyword) {
        List<Object> result = new ArrayList<>();
        result.addAll(makeDynastyResponse(keyword, searchDynasties(keyword)));
        result.addAll(makeKingsResponse(keyword, searchKings(keyword)));
        return result;
    }


    @Override
    public List<InfoKing> searchKings(String keyword) {
        if (kingsSearchCache.isEmpty()) {
            TypeReference<List<InfoKing>> typeReference = new TypeReference<List<InfoKing>>() {
                @Override
                public Type getType() {
                    return super.getType();
                }
            };

            kingsSearchCache.addAll(fromFile("kings", typeReference));
        }
        List<InfoKing> result = new ArrayList<>();
        if (isVietnameseCharacter(keyword)) {
            result.addAll(kingsSearchCache.stream()
                    .filter(infoKing ->  isMatchWithVICharacter(infoKing.getEmperorName(), keyword))
                    .collect(Collectors.toList()));
        } else {
            result.addAll(kingsSearchCache.stream()
                    .filter(infoKing -> isMatch(infoKing.getEmperorName(), keyword))
                    .collect(Collectors.toList()));
        }
        return makeKingsResponse(keyword, result);
    }

    public void indexDataFromWeb() throws IOException {
        String link = "https://vi.wikipedia.org/wiki/Vua_Vi%E1%BB%87t_Nam";
        Document document = Jsoup.connect(link).get();
        List<InfoKing> infoKings = contentService.getAllInfoKing(document);
        List<Dynasty> dynasties = contentService.getAllDynasty(document);
        toFile("dynasties", dynasties);
        toFile("kings", infoKings);
        Data dataToSave = new Data(infoKings, dynasties);
        toFile("data", dataToSave);
    }


}
