package edu.hanoi.services.impl;

import edu.hanoi.model.Dynasty;
import edu.hanoi.model.HistoryDynasty;
import edu.hanoi.model.InfoKing;
import edu.hanoi.services.ContentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@RequiredArgsConstructor
@Slf4j
public class ContentServiceImpl implements ContentService {
    public static final String REGEX1 = "\\[(.*?)]";

    public static final String REGEX2 = "\\((.*?)\\)";

    @Override
    public List<InfoKing> getAllInfoKing(Document document) {
        List<InfoKing> result = new ArrayList<>();
        infoKing(document).forEach((key, infoKing) -> {
            infoKing.setEmperorName(key);
            result.add(infoKing);
        });

        return result;
    }
    @Override
    public List<Dynasty> getAllDynasty(Document document) {
        Map<String, HistoryDynasty> historyDynastyMap = historyDynasty(document);
        List<Dynasty> dynasties = new ArrayList<>();

        historyDynastyMap.forEach((key, historyDynasty) -> {
            Dynasty dynasty = new Dynasty();
            dynasty.setKing(historyDynasty.getKing());
            dynasty.setTime(historyDynasty.getTime());
            dynasty.setNameDynasty(key);
            dynasties.add(dynasty);
        });

        return dynasties;
    }

    /**
        TRIỀU ĐẠI ---> {
            THỜI GIAN,
            VUA
        }
     */
    private Map<String, HistoryDynasty> historyDynasty(Document document) {
        Map<String, HistoryDynasty> historyDynastyMap = new HashMap<>();
        Elements elmsHistoryLine = document.getElementsByTag("h3");
        Elements elmsHistoryDynasty = document.select("table[cellpadding='0']");
        elmsHistoryDynasty.remove(10);
        for (int i = 0; i < elmsHistoryLine.size(); i++) {
            String elmsHistoryLineToString = elmsHistoryLine.get(i).text().replaceAll(REGEX1, "");
            String time = "Không rõ";
            List<String> king = new ArrayList<>();
            Elements elmsKing = elmsHistoryDynasty.get(i).getElementsByTag("b");
            Pattern p = Pattern.compile(REGEX2);
            Matcher m = p.matcher(elmsHistoryLineToString);
            while (m.find()) {
                time = m.group(1);
            }
            for (org.jsoup.nodes.Element element : elmsKing) {
                king.add(element.text());
            }
            historyDynastyMap.put(elmsHistoryLineToString.replaceAll(REGEX2, ""), new HistoryDynasty(time, king));
        }
        return historyDynastyMap;
    }

    /**
        VUA ---> {
            NIÊN HIỆU,
            TÊN HUÝ,
           THẾ THỨ,
          TRỊ VÌ
         }
     */
    private Map<String, InfoKing> infoKing(Document document) {
        Map<String, InfoKing> infoKingMap = new HashMap<>();
        int indexStateBrand = 0;
        int indexName = 0;
        int indexInformation = 0;
        int indexTime = 0;
        Elements elmsHistoryDynasty = document.select("table[cellpadding='0']");
        elmsHistoryDynasty.remove(10);
        for (org.jsoup.nodes.Element element : elmsHistoryDynasty) {
            Elements elmsKingName = element.getElementsByTag("b");
            Elements elmsKing = element.getElementsByTag("tr");
            Elements elmsIndexInfoKing = elmsKing.get(0).getElementsByTag("th");
            for (int j = 0; j < elmsIndexInfoKing.size(); j++) {
                String regex = elmsIndexInfoKing.get(j).text().replaceAll(REGEX1, "");
                if (("Niên hiệu").equals(regex)) {
                    indexStateBrand = j;
                }
                if (("Tên húy").equals(regex)) {
                    indexName = j;
                }
                if (("Thế thứ").equals(regex)) {
                    indexInformation = j;
                }
                if (("Trị vì").equals(regex)) {
                    indexTime = j;
                }
            }
            elmsKing.remove(0);
            for (int j = 0; j < elmsKing.size(); j++) {
                InfoKing infoKing = new InfoKing();
                Elements elmsInfoKing = elmsKing.get(j).getElementsByTag("td");
                if (indexStateBrand != 0) {
                    infoKing.setStateBrand(elmsInfoKing.get(indexStateBrand).text().replaceAll(REGEX1, ""));
                }
                if (indexName != 0) {
                    infoKing.setName(elmsInfoKing.get(indexName).text().replaceAll(REGEX1, ""));
                }
                if (indexInformation != 0) {
                    infoKing.setInformation(elmsInfoKing.get(indexInformation).text().replaceAll(REGEX1, ""));
                }
                if (indexTime != 0 && indexTime + 1 < elmsInfoKing.size()) {
                    infoKing.setTime((elmsInfoKing.get(indexTime).text() + " " + elmsInfoKing.get(indexTime + 1).text() + " " + elmsInfoKing.get(indexTime + 2).text()).replaceAll(REGEX1, ""));
                } else if (indexTime != 0) {
                    infoKing.setTime(elmsInfoKing.get(indexTime).text().replaceAll(REGEX1, ""));
                }
                infoKingMap.put(elmsKingName.get(j).text(), infoKing);
            }
            indexStateBrand = 0;
            indexName = 0;
            indexInformation = 0;
            indexTime = 0;
        }
        return infoKingMap;
    }

}
