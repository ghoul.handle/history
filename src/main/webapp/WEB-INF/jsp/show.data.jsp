<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@page language="java" pageEncoding="UTF-8" %>
<div id="root" class="root">
  <div id="flex1" class="flex1">
    <img class="image-background" src="/resources/image/hih.jpg">
  </div>
  <div id="flex2" class="flex2">
    <div id="flex2-1" class="flex2-1">
      <p class="p1">LẤY DỮ LIỆU THÀNH CÔNG</p>
    </div>
    <div id="flex2-2" class="flex2-2">
      <div id="flex2-2-1" class="flex2-2-1">
        <select name="choose" id="choose" class="choose">
          <option value="all">Tất cả</option>
          <option value="dynasty">Triều đại</option>
          <option value="king">Vua</option>
        </select>
        <input id="loader-text" type="text" class="loader-text" placeholder="Nhập từ khóa để tìm kiếm (keyword)"/>
      </div>
      <div id="flex2-2-2" class="flex2-2-2">
        <button id="search" class="search">Tìm kiếm</button>
      </div>
      </div>
    <div id="flex2-3" class="flex2-3">
      <p id="no-data" class="no-data data">Không có dữ liệu phù hợp</p>
      <div id="information" class="information">
      </div>
    </div>
  </div>
</div>
