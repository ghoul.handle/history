<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="tilesx" uri="http://tiles.apache.org/tags-tiles-extras" %>
<%@page language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Spring Boot Application</title>
	<tilesx:useAttribute id="listCss" name="stylesheets" classname="java.util.List"/>
	<c:forEach var="item" items="${listCss}">
		<link rel="stylesheet" href="${pageContext.request.contextPath}${item}"/>
	</c:forEach>
</head>
<body>
	<tiles:insertAttribute name="body"/>
	<tilesx:useAttribute id="listJs" name="scripts" classname="java.util.List"/>
	<c:forEach var="item" items="${listJs}">
		<script src="${pageContext.request.contextPath }${item}"></script>
	</c:forEach>
</body>
</html>