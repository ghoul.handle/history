<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@page language="java" pageEncoding="UTF-8" %>
<div id="root" class="root">
    <div id="flex1" class="flex1">
        <img class="image-background" src="/resources/image/hih.jpg">
    </div>
    <div id="flex2" class="flex2">
        <div id="flex2-1" class="flex2-1">
            <p class="p1">CHÀO MỪNG ĐẾN VỚI WEB</p>
            <p class="p2">THU THẬP DỮ LIỆU LỊCH SỬ</p>
        </div>
        <div id="flex2-2" class="flex2-2">
            <div id="loader" class="loader">
                <div class="inner one"></div>
                <div class="inner two"></div>
                <div class="inner three"></div>
            </div>
            <button id="loader-text" class="loader-text">
                THU THẬP DỮ LIỆU
            </button>
        </div>
        <div id="flex2-3" class="flex2-3">
        </div>
    </div>
</div>
