var $loaderText, $loader

$(document).ready(function () {
    getId();
    handle();
});

function getId() {
    $loader = $("#loader-text");
    $loaderText = $("#loader")
}

function handle() {
    $loader.on('click',function (){
        $loader.hide();
        $loaderText.show();
        getData();
    })
}

function getData() {
    $.ajax({
        url : "/api/data",
        type : "GET"
    }).done(function (data) {
        if (data === true) {
            window.location = "/data?isSuccess=" + data;
        }
    })
}

