var $loaderText, $search, $information, $noData, $choose;
var keyword, choose, dynasty, king;

$(document).ready(function () {
    getId();
    handle();
});

function getId() {
    $loaderText = $("#loader-text");
    $search = $("#search");
    $information = $("#information");
    $noData = $("#no-data");
    $choose = $("#choose");
    choose = $choose.val();
    keyword = "";
}

function handle() {
    $loaderText.on('input', function () {
       keyword = $(this).val();
    })

    $choose.focusout(function () {
        choose = $choose.val();
    })

    $search.on('click', function () {
        $information.text("");
        searchData(keyword);
    })

    $loaderText.keypress(function (event) {
        if (event.keyCode == 13 || event.which == 13) {
            $information.text("");
            searchData(keyword);
        }
    })
}

function searchData(keyword) {
    $.ajax({
        type: 'GET',
        url: `http://localhost:8080/api/search?keyword=${keyword}`
    }).done(function (data) {
        console.log(data)
        if (data.length !== 0) {
            $noData.hide();
            if (choose === 'all') {
                showDynasty(data);
                showKing(data);
            } else if (choose === 'dynasty') {
                showDynasty(data);
                if (!dynasty) {
                    $noData.show();
                }
            } else {
                showKing(data);
                if (!king) {
                    $noData.show();
                }
            }
        } else {
            $noData.show();
        }
    })
}

function showDynasty(data) {
    for (const element of data) {
        if (element.nameDynasty !== undefined) {
            dynasty = true;
            break;
        } else {
            dynasty = false;
        }
    }
    if (dynasty) {
        $information.append(`
                        <h1>TRIỀU ĐẠI</h1>
                    `)
    }
    for (const element of data) {
        if (element.nameDynasty !== undefined) {
            let nameDynasty = element.nameDynasty.replace(element.dataMatch, `<span style='color: burlywood;'>${element.dataMatch}</span>`);
            $information.append(`
                        <div class="data">
                            <div class="label"><b>${nameDynasty}:</b></div>
                            <div><b>Vua:</b> ${element.king}</div>
                            <div><b>Khoảng thời gian tồn tại:</b> ${element.time}</div>
                        </div>
                    `)
        }
    }
}

function showKing(data) {
    for (const element of data) {
        if (element.emperorName !== undefined) {
            king = true;
            break;
        } else {
            king = false;
        }
    }
    if (king) {
        $information.append(`
                        <h1>CÁC VỊ VUA</h1>
                    `)
    }
    for (const element of data) {
        if (element.emperorName !== undefined) {
            let nameEmperor = element.emperorName.replace(element.dataMatch, `<span style='color: burlywood;'>${element.dataMatch}</span>`);
            $information.append(`
                        <div class="data">
                            <div class="label"><b>${nameEmperor}:</b></div>
                            <div><b>Tên húy:</b> ${element.name}</div>
                            <div><b>Niên hiệu:</b> ${element.stateBrand}</div>
                            <div><b>Thời gian trị vì:</b> ${element.stateBrand}</div>
                            <div><b>Thông tin thêm:</b> ${element.information}</div>
                        </div>
                    `)
        }
    }
}