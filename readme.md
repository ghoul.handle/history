Tool kéo data từ web
===================================
Trước khi chạy app cần thay thư mục lưu file trong application.properties

## Kéo data về file:
* B1: vào trình duyệt web gõ localhost:8080
* B2 :....

### Curl API search các triều đại: 
```
curl --location --request GET 'localhost:8080/api/search/dynasties?keyword=hoặc' \
--header 'Cookie: JSESSIONID=2421F59AC868E8090034D7A575BE7908'
```

### Curl API search Kings:

```
curl --location --request GET 'localhost:8080/api/search/kings?keyword=khoáng' \
--header 'Cookie: JSESSIONID=2421F59AC868E8090034D7A575BE7908'
```
### Curl API search all:
```
curl --location --request GET 'localhost:8080/api/search?keyword=a' \
--header 'Cookie: JSESSIONID=2421F59AC868E8090034D7A575BE7908'
```